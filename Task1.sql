CREATE TABLE Branch (
    branch VARCHAR(250) PRIMARY KEY,
	Branch_Addr TEXT
);

CREATE TABLE Book (
    isbn INT PRIMARY KEY,
    title VARCHAR(255),
    author VARCHAR(255),
    publisher VARCHAR(255),
    num_copies INT,
    branch VARCHAR(250),
    FOREIGN KEY (branch)
		REFERENCES Branch(branch)
);
