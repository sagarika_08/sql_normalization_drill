 CREATE TABLE Manager (
    manager_id INT PRIMARY KEY,
    Manager_name VARCHAR(255),
    Manager_location VARCHAR(255)
);

CREATE TABLE Staff (
    staff_id INT PRIMARY KEY,
    Staff_name VARCHAR(255),
    Staff_location VARCHAR(255),
    manager_id INT,
    FOREIGN KEY (manager_id)
    REFERENCES Manager (manager_id)
);
CREATE TABLE Contract (
    contract_id INT PRIMARY KEY,
    Estimated_cost DOUBLE,
    Completion_date date,
    manager_id INT,
    FOREIGN KEY (manager_id)
    REFERENCES Manager (manager_id)
);
CREATE TABLE Client (
    client_id INT PRIMARY KEY,
    Name VARCHAR(255),
    Location VARCHAR(255),
    contract_id INT,
    FOREIGN KEY (contract_id)
    REFERENCES Contract (contract_id)
);