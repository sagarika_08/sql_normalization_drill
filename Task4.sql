
CREATE TABLE Secretary (
    secretary_id INT PRIMARY KEY,
    Name VARCHAR(255)
);
CREATE TABLE Doctor (
    doctor_id INT PRIMARY KEY,
    Name VARCHAR(255)
);
CREATE TABLE Patient (
    patient_id VARCHAR(255) PRIMARY KEY,
    Name VARCHAR(255),
    dob DATE,
    address TEXT
);
CREATE TABLE Prescription (
    prescription_id VARCHAR(255) PRIMARY KEY,
    drug VARCHAR(255),
    dosage VARCHAR(255),
    prescription_date DATE,
    patient_id INT,
    FOREIGN KEY (patient_id)
	REFERENCES Patient (patient_id),
    doctor_id INT,
    FOREIGN KEY (doctor_id)
	REFERENCES Doctor (doctor_id)
);
